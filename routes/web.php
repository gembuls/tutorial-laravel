<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', function () {
    echo "<h1>Home with Echo</h1>";
});


Route::get('/home/about', [\App\Http\Controllers\HomeController::class, 'about']);
Route::get('/home/contact', [\App\Http\Controllers\HomeController::class, 'contact']);
Route::get('/home/datapribadi', [\App\Http\Controllers\HomeController::class, 'datapribadi']);

Route::get('/job-families/add-data', [\App\Http\Controllers\JobFamiliesController::class, 'addData']);
Route::get('/job-families/update-data', [\App\Http\Controllers\JobFamiliesController::class, 'updateData']);
Route::get('/job-families/show-data/{id}/{judul}', [\App\Http\Controllers\JobFamiliesController::class, 'showData']);
Route::get('/job-families/delete-data', [\App\Http\Controllers\JobFamiliesController::class, 'deleteData']);