<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\JobFamilies;

class JobFamiliesController extends Controller
{
	
	function addData()
	{
		$model = new JobFamilies;
		
		$model->name = "Human Capital Management";
		$model->number = 10;
		$model->job_family_code = "HCM10";
		$model->description = "Job Family HCM";
		$model->level_description = "Job Family";
		$model->level = 1;
		
		$model->save();
	}
	
	function updateData()
	{
		$model = JobFamilies::where('id', 1)->first();
		
		if(empty($model))
		{
			echo "Data tidak ditemukan";
			exit;
		}
				
		$model->name = "Marketing";
		$model->save();
	}
	
	function showData($id, $judul)
	{
		var_dump($judul);
		$model = JobFamilies::where('id', 1)->first();
		
		if(empty($model))
		{
			echo "Data tidak ditemukan";
			exit;
		}
				
		return view('job-families.show-data', compact('model'));
	}
	
	function deleteData()
	{
		$model = JobFamilies::where('id', 1)->first();
		
		if(!empty($model))
			$model->delete();
		
	}
	
}