<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function about()
    {
        echo "About";
    }
	
	public function contact()
    {
        return "Nomor Telpon: 0811923123";
    }
	
	public function datapribadi()
    {
        return view('home.datapribadi');
    }
	
}